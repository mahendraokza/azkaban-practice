from azkaban import Job, Project

PROJECT = Project(name="azkaban_cli_examples", root=__file__)

PROJECT.properties = {"number": 5, "rank": 3}

JOBS = {
    "startnoop": Job(
        {
            "type": "noop",
        }
    ),
    "hello_world_step.cmd": Job(
        {
            "type": "command",
            "command": 'echo "hello world! javascript process incoming!"',
            "dependencies": "startnoop"
        }
    ),
    "chmod_echo.cmd": Job(
        {
            "type": "command",
            "command": "chmod 777 echo.sh",
            "dependencies": "hello_world_step.cmd"
        }
    ),
    "chmod_call_js.cmd": Job(
        {
            "type": "command",
            "command": "chmod 777 call_js.sh",
            "dependencies": "hello_world_step.cmd"
        }
    ),
    "check_properties_param.cmd": Job(
        {
            "type": "command",
            "command": "./echo.sh ${number} ${rank}",
            "dependencies": "chmod_echo.cmd",
        }
    ),
    "js_step_1.cmd": Job(
        {
            "type": "command",
            "command": "./call_js.sh ${number} ${rank}",
            "dependencies": "chmod_call_js.cmd",
        }
    ),
    "close_step.cmd": Job(
        {
            "type": "command",
            "command": 'echo "JS has been called! the process has done!',
            "dependencies": "check_properties_param.cmd,js_step_1.cmd",
        }
    ),
    "endnoop": Job(
        {
            "type": "noop",
            "dependencies": "close_step.cmd"
        }
    )
}

for name, job in JOBS.items():
    PROJECT.add_job(name, job)

FILES = {
    './call_js.sh': 'call_js.sh',
    './echo.sh': 'echo.sh',
    './js_example.js': 'js_example.js'
}

for file, name in FILES.items():
    PROJECT.add_file(file, name)