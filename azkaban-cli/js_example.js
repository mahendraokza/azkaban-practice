const pow = (number, rank) => {
    if(rank===0){
        return 1
    }
    return number * pow(number, rank-1) 
}

number = parseInt(process.argv[2]) //because interpreted as string
rank = process.argv[3]
console.log(`Pow from ${number}^${rank} = ${pow(number, rank)}`)